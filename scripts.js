function dragstart_handler(eventDragStart) {
    console.log("Draging is started");;
    eventDragStart.dataTransfer.effectAllowed = "copy";
    eventDragStart.currentTarget.style.backgroundColor = "green";
    eventDragStart.dataTransfer.setData("text/plain", eventDragStart.target.id);
}

function drag_enter(dragEnter) {
    eventDragover.preventDefault();
    if (dragEnter.target.className == "draggableArea") {
        console.log("This area is draggable !!!")
    } else {
        console.log("This area is not draggable !!!")
    }
}

function drag_over(eventDragover) {
    if (eventDragover.target.id != "target4") {
        console.log("This area is draggable !")
        eventDragover.preventDefault();
    } else {
        console.log("This area is not draggable !")
    }

}

function drag_end(eventDragEnd) {
    console.log("The dragged item is about to drop drop !!!");
}

function drop_handler(eventDrop) {
    console.log("The item is dropped !");
    eventDrop.preventDefault();
    var data = eventDrop.dataTransfer.getData("text");
    eventDrop.target.appendChild(document.getElementById(data));
    eventDrop.dataTransfer.clearData();
}